local item = ...

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        hero_animation = "sf_weapon_axe_swing",
        weapon_sprite = "solforge_weapons/axe_1",
        weapon_animation = "axe",
        weapon_sound = "sword1",
      },
      {
        hero_animation = "sword_swing_backhand",
        weapon_sprite = "solforge_weapons/axe_1",
        weapon_animation = "axe_reverse",
        weapon_sound = "sword1",
      },
    },

    weapon_parameters = {
      attack_power = 6,
      --cost_type = "life",
      --cost_amount = 1,
      --damage_type = "fire",
      weight = 10,
    }
  }
)
