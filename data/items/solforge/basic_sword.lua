local item = ...

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  local m = sol.movement.create("straight")
  local direction = hero:get_direction()
  m:set_angle(direction * math.pi / 2)
  m:set_max_distance(4)
  m:start(hero)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        hero_animation = "sword_swing",
        weapon_sprite = "hero/sword1",
        weapon_animation = "sword",
        weapon_sound = "sword1",
        callback = step_forward,
      },
      {
        hero_animation = "sword_swing_backhand",
        weapon_sprite = "hero/sword1",
        weapon_animation = "sword_backhand",
        weapon_sound = "sword1",
        callback = step_forward,
      },
      {
        hero_animation = "sword_swing",
        weapon_sprite = "hero/sword1",
        weapon_animation = "spin_attack",
        weapon_sound = "sword1",
        attack_power_bonus = 2,
      },
    },


    weapon_parameters = {
      attack_power = 2,
    },

  }
)
