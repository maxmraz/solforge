local item = ...

local step_forward = function()
  local hero = sol.main.get_game():get_hero()
  local m = sol.movement.create("straight")
  local direction = hero:get_direction()
  m:set_angle(direction * math.pi / 2)
  m:set_max_distance(8)
  m:start(hero)
end

require("items/solforge/lib/forge"):temper_weapon(item,
  {
    item_id = item:get_name():gsub("/", "_"),

    attacks = {
      {
        hero_animation = "spear_attack",
        weapon_sprite = "solforge_weapons/spear_1",
        weapon_animation = "spear",
        weapon_sound = "sword4",
        callback = step_forward,
      },
      {
        hero_animation = "spear_attack",
        weapon_sprite = "solforge_weapons/spear_1",
        weapon_animation = "spear",
        weapon_sound = "sword4",
        callback = step_forward,
      },
      {
        hero_animation = "spear_attack",
        weapon_sprite = "solforge_weapons/spear_1",
        weapon_animation = "spear",
        weapon_sound = "sword4",
        callback = step_forward,
      },
      {
        hero_animation = "spear_attack_heavy",
        weapon_sprite = "solforge_weapons/spear_1",
        weapon_animation = "heavy_attack",
        weapon_sound = "sword4",
      },
    },



    weapon_parameters = {
      attack_power = 2,
      --cost_type = "life",
      --cost_amount = 1,
      --damage_type = "fire",
      weight = 10,
      durability = 100,
    }
  }
)
