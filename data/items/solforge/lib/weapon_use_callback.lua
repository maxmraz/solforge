--[[
Created by Jeff Cournoyer, licensed under the MIT License.
This script is meant to be a catch-all for effects one may want to
activate when weapons are used, in addition to the default effects 
present in the forge.lua file. This could be used for, as an example,
adding a flame tail sprite to the fire sword's swing. 
--]]

local weapon_use_manager = {}


  --Adds effects to the weapon when using the weapon.
  function weapon_use_manager:initiate_usage(item)
    local game = item:get_game()
    local hero = game:get_hero()
    local map = game:get_map()

    --Insert any action or effect code here to add to the default item:on_using event.

    --Debugging below.
    if item.item_id == "solforge_basic_fire_sword" then
      local hero_weapon_sprite = hero.weapon_entity:get_sprite()
      --Changes the fire sword's sprite to a bright red hue.
      hero_weapon_sprite:set_color_modulation{255,55,55,255}
    end

  end

return weapon_use_manager