--Created by Max Mraz and J. Cournoyer, licensed with the MIT license
--This script manages what happens when a solforge weapon hits something.
--Alter this script in whatever way suits your game, default behavior provided will hurt enemies
--The solforge weapon is passed to the process_collision function as item, and the attack that collided as attack

local manager = {}

function manager:process_collision(props)
  local game = props.game
  local map = props.map
  local hero = props.hero
  local item = props.item --the item itself
  local attack = props.attack --the current attack
  local weapon_entity = props.weapon_entity --the custom entity of the weapon
  local other_entity = props.other_entity --the entity registering the sprite collision
  local weapon_sprite = props.weapon_sprite --the sprite of the weapon entity
  local other_sprite = props.other_sprite --the sprite of the other entity


  --Allow any entity to define its own behavior when getting hit:
  --For example, if you have a custom entity that can be destroyed, you can use this method
  if other_entity.react_to_solforge_weapon then other_entity:react_to_solforge_weapon(item) end


  --ENEMY
  --In this implementation, I'm expecting weapon attacks to have an `attack_power_bonus` value to be added to the attack power.
  --That's the case with some default weapons included, but you can just as easily create weapons that have a combo_percentage value,
  --Which adds a percent of the attack_power savegame value, so its as effective as you upgrade.
  --Or you could have a ohko_chance that increases every attack, which is a percent chance to just kill the enemy.
  --Point is, you can add any arbitrary value to attacks or to the weapons that you want available when hitting stuff.
  if other_entity:get_type() == "enemy" then
    local attack_power = (game:get_value(item.item_id .. "_attack_power") or 1) + (attack.attack_power_bonus or 0)

    --The following line will update the durability before dealing damage to the enemy. 
    manager:check_durability(game, item.item_id, other_entity)

    --Following function will take into account enemy's attack consequence settings,etc.
    manager:initiate_attack_consequence(other_entity, item.item_id, weapon_entity, attack_power)

   --Check if the enemy should push the hero back when struck; if so, move hero away from enemy slightly.
    if other_entity:get_push_hero_on_sword() then
      hero:stop_movement()
      local m = sol.movement.create"straight"
      m:set_speed(128)
      m:set_max_distance(64)
      m:set_angle(other_entity:get_angle(hero))
      m:start(hero)
    end

  end


  --DESTRUCTIBLE
  if other_entity:get_type() == "destructible" then
    local x,y,z = other_entity:get_position()

    --If it blows up, explode it
    if other_entity:get_can_explode() then
      map:create_explosion{x=x, y=y, layer=z}
      sol.audio.play_sound"explosion"
      if other_entity.on_exploded then other_entity:on_exploded() end
      other_entity:remove()

    --If it can be cut, cut it
    elseif other_entity:get_can_be_cut() then
      if other_entity:get_destruction_sound() then
        sol.audio.play_sound(other_entity:get_destruction_sound())
      end
      if other_entity:get_treasure() then
        local tname, tvar, tsave = other_entity:get_treasure()
        map:create_pickable{
          x=x, y=y, layer=z,
          treasure_name = tname, treasure_variant = tvar, treasure_savegame_variable = tsave,
        }
      end
      if other_sprite:has_animation("destroy") then
        other_sprite:set_animation("destroy", function()
          other_entity:remove()
        end)
      else
        other_entity:remove()
      end
      if other_entity.on_cut then other_entity:on_cut() end
    end
  end


  --SWITCH
  --Note: due to lack of accessor methods, there's no way to know if a switch is solid or arrow type
  --Therefore, this toggles ALL switches (other than walkable), even arrow type
  --Arrow type is a bit depricated anyway, as they only respond to built-in arrows. Custom arrows have no way to know either.
  if other_entity:get_type() == "switch"
    and not other_entity:is_walkable() then
    local switch = other_entity
    sol.audio.play_sound("switch")
    switch:set_activated(not switch:is_activated())
    if switch:is_activated() then
      other_sprite:set_animation("activated")
      if switch.on_activated then switch:on_activated() end
    else
      other_sprite:set_animation("inactivated")
      if switch.on_inactivated then switch:on_inactivated() end
    end
  end


  --Flip Crystal switches
  if other_entity:get_type() == "crystal" and not other_entity.solforge_activation_cooldown then
    sol.audio.play_sound("switch")
    map:change_crystal_state()
  end

end

--Takes an intial argument, game, that is the game object
--that the item exists within.
--Takes a secondary argument, weapon, that is the colliding weapon.
--weapon is a string that is equal to the item.item_id.
--Takes a tertiary argument, other_entity, that is the entity the weapon is colliding with.
--Checks the durability of the weapon
function manager:check_durability(game, weapon, other_entity)
  local durability = game:get_value(weapon .. "_durability")    ---Think of this as the Health Points of the Weapon.
  local degradation = game:get_value(weapon .. "_degradation")    ---This is the amount of "damage" the Durability takes on collision.

  if durability ~= nil then
    if degradation == nil then 
      game:set_value(weapon .. "_degradation", 0)
      degradation = game:get_value(weapon .. "_degradation")
    end
    local new_durability = durability - degradation
    if new_durability < 0 then new_durability = 0 end
    game:set_value((weapon .. "_durability"), (new_durability))

  end
end


function manager:initiate_attack_consequence(enemy, item_id, weapon_entity, attack_power)
  local game = enemy:get_game()
  local hero = game:get_hero()
  local attack_consequence = enemy:get_attack_consequence"sword"

  if enemy:get_breed():match("shadblow_enemies") then --special check, as shadblow enemies don't use enemy:hurt()
    enemy:process_hit(attack_power)
  elseif type(attack_consequence) == "number" then
    enemy:hurt(attack_power)
  elseif type(attack_consequence) == "function" then
    attack_consequence()
  else
    --If the attack_consequence is not a number or a function, then it is a string.
    if attack_consequence == "ignored" then
      --Do nothing. Enemy ignores the collision and damage.
    elseif attack_consequence == "protected" then
      if game:get_value(item_id .. "_attack_blocked_animation") and game:get_value(item_id .. "_attack_blocked_sound") then 
        weapon_entity:get_sprite():set_animation(game:get_value(item_id .. "_attack_blocked_animation"))
        sol.audio.play_sound(game:get_value(item_id .. "_attack_blocked_sound"))
      end
    elseif attack_consequence == "immobilized" then
      enemy:immobilize()
    elseif attack_consequence == "custom" then
      enemy:hurt(attack_power)
    end   --End the string if block.

  end   --End the type() if block.

end






return manager